#include <stdlib.h>
#include <stdint.h>
#include "ssd_OLED/ssd1306.h"
#include "ssd_OLED/fonts.h"	//include it if u want text

#include "avrMain.h"

/*
 * This is just example how to run code on pc.
 */

int avrMain(void)
{
	cout("You can print me to the console!");
	cout("And I should be reimplemented to work as printf");

	while (1)
	{
		ssd1306_string(2,60,"asdfASDF",2,2,2,src_RAM);
		ssd1306_rectangleFilled(5,10,20,15,2);
		ssd1306_display();
		_delay_ms(500);
	}
}
/*
 * AVR version - delete ssd_OLED/emulator and you could
 * rename this file to main.
 *
#include <util/delay.h>
#include <stdlib.h>
#include <stdint.h>
#include "ssd_OLED/ssd1306.h"
#include "ssd_OLED/fonts.h"	//include it if u want text
int main(void)
{
	ssd1306_init(SSD1306_SWITCHCAPVCC,SSD1306_REFRESH_MID,0);
	while (1)
	{
		ssd1306_string(2,60,"asdfASDF",2,2,2,src_RAM);
		ssd1306_rectangleFilled(5,10,20,15,2);
		ssd1306_display();
		_delay_ms(500);
	}
}
*/
