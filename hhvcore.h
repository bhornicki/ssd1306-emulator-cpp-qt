#ifndef HHVCORE_H
#define HHVCORE_H


#ifndef abs
	#define abs(x) ((x<0)?-x:x)
#endif
#ifndef sgn
	#define sgn(x) ((x > 0) ? 1 : ((x < 0) ? -1 : 0))
#endif
#ifndef min
	#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#if !defined(__AVR__)
	#warning No AVR selected, using PC emulation

	void eeprom_read_block(void * __dst, const void * __src, size_t __n);
	void eeprom_update_block(const void * __src, void * __dst, size_t __n);
	uint8_t eeprom_read_byte(const uint8_t * __p);
	void eeprom_update_byte(uint8_t * __p,uint8_t __value);

	#define pgm_read_byte *
	#define pgm_read_word *
	#define pgm_read_dword *
	#define PSTR
	#define PROGMEM
	#define EEMEM


#endif

//Set pgm_read_pointer (used by fonts) to work with width predicted by architecture - useful with certain MCUs
#if !defined(__INT_MAX__) || (__INT_MAX__ > 0xFFFF)
	 #define pgm_read_pointer(addr) ((void *)pgm_read_dword(addr))
#else
	 #define pgm_read_pointer(addr) ((void *)pgm_read_word(addr))
#endif




#define src_RAM		0
#define src_FLASH	1
#define src_EEPROM	2
uint8_t readByteFrom(const uint8_t *memory, uint8_t type);

uint8_t bcdDec(uint8_t bcd);
uint8_t decBcd(uint8_t dec);



#endif // HHVCORE_H
