#include <stdint.h>
#include <string.h>
#if defined(__AVR__)
	#include <avr/eeprom.h>
	#include <avr/pgmspace.h>
#endif

#include "hhvcore.h"

#if !defined(__AVR__)
	void eeprom_read_block(void * __dst, const void * __src, size_t __n)
	{
		memcpy(__dst,__src,__n);
	}
	void eeprom_update_block(const void * __src, void * __dst, size_t __n)
	{
		memcpy(__dst,__src,__n);
	}
	uint8_t eeprom_read_byte(const uint8_t * __p)
	{
		return *__p;
	}
	void eeprom_update_byte(uint8_t * __p,uint8_t __value)
	{
		*__p=__value;
	}
#endif


uint8_t readByteFrom(const uint8_t *memory, uint8_t type)
{

	switch (type)
	{
		case src_FLASH:
			return (uint8_t) pgm_read_byte(memory);
		case src_EEPROM:
			return (uint8_t) eeprom_read_byte(memory);

		default:
			if(!memory)
				return 0;
			return (uint8_t)*memory;
	}
}


uint8_t bcdDec(uint8_t bcd) {
	return ((((bcd) >> 4) & 0x0F) * 10) + ((bcd) & 0x0F);
}
uint8_t decBcd(uint8_t dec) {
	return ((dec / 10) << 4) | (dec % 10);
}
