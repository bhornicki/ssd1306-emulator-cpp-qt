#include <istream>
#include <cstdlib>
#include <QString>


#include "myThread.h"

#include "emulator.h"
uint8_t buttons;

extern "C" {		//Jaka zajebista konstrukcja <3
#include "../graphics_mono.h"
}

myThread *trd;
void myThreadInit(void *src)
{
	trd=(myThread *)src;
}
void cout(char msg[])
{
	trd->cout(QString::fromLocal8Bit(msg));
}
void _delay_ms(unsigned long time)
{
	trd->msleep(time);
}
void _delay_s(unsigned long time)
{
	trd->sleep(time);
}
void ssd1306_displayLine(uint8_t start, uint8_t stop)
{
	if (stop > 7) stop = 7;
	if (start > stop) return;
	while (start<=stop) {
		emit trd->displayEm((quint8*)SSD1306_frameBuff,(quint8)start++);
	}
}
void ssd1306_display(void)
{
	ssd1306_displayLine(0,7);
	_delay_ms(3);
}
void ssd1306_init(uint8_t vcc, uint8_t refresh, uint8_t invertDisp){
	vcc=refresh=invertDisp=vcc;
}
