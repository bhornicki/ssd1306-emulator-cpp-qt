Project is powered by QT framework.
copy "oledEmulator.pro" to Your main folder and use it to configure QT project.

Use the following structure:
project folder
* oledEmulator.pro
* avrMain.c
* * #include "ssd_OLED/ssd1306.h"
* * int avrMain(void) definition here
* avrMain.h
* * int avrMain(void) declaration here
* other .c files as You want
* ssd_OLED folder
* * emulator folder
