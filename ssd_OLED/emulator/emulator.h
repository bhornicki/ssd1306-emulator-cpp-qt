#ifndef EMULATOR_H
#define EMULATOR_H



#ifdef __cplusplus
extern "C" {
#endif
void myThreadInit(void *src);

#define b_up 1<<0
#define b_down 1<<1
#define b_mid ((1<<2)|(1<<3))
extern uint8_t buttons;

void ssd1306_init(uint8_t vcc, uint8_t refresh, uint8_t invertDisp);
#define SSD1306_REFRESH_MIN 0x80
#define SSD1306_REFRESH_MID 0xB0
#define SSD1306_REFRESH_MAX	0xF0
#define SSD1306_EXTERNALVCC 0x1
#define SSD1306_SWITCHCAPVCC 0x2

void cout(char msg[]);
void _delay_ms(unsigned long time);
void _delay_s(unsigned long time);
void ssd1306_displayLine(uint8_t start, uint8_t stop);
void ssd1306_display(void);

#ifdef __cplusplus
}
#endif
#endif // EMULATOR_H
