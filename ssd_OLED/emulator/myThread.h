#ifndef myThread_H
#define myThread_H

#include <QObject>
#include <QThread>
//#include "widget.h"

class myThread : public QThread
{
	 Q_OBJECT
public:
	myThread();
	~myThread();
signals:
    void displayEm(quint8 *buf,quint8 pag);
    void cout(QString msg);
public slots:
    void buttonPressed(quint8 butt);
    void processCmd(QString msg);
private:
    void run();
};

#endif // myThread_H
