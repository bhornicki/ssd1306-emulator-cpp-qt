#include "widget.h"
#include "ui_widget.h"
#include <QtWidgets>
#include <QPainter>
#include "myThread.h"

Widget::Widget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Widget)
{
	ui->setupUi(this);
	pix=new QPixmap(128,64);
	pix->fill(Qt::black);

	ui->output->setTabStopWidth(7*QFontMetrics(ui->output->currentCharFormat().font()).width(' '));

	trd= new myThread();
	connect(trd,SIGNAL(displayEm(quint8*,quint8)),this,SLOT(updateDisplay(quint8*,quint8)));	//po��cz ssd1306_displayLine z funkcj� wykonawcz�
	connect(this,SIGNAL(processTextEmit(QString)),this,SLOT(processText(QString)));				//napisz tekst w QTextBrowser z tego pliku
	connect(trd,SIGNAL(cout(QString)),this,SLOT(processText(QString)));							//umo�liw wy�wietlanie wiadomo�ci z myThreada
	trd->start();
}

Widget::~Widget()
{
	trd->terminate();
	delete trd;
	delete pix;
	delete ui;
}


/*
 * Zapisz pozycj� kursora po wci�ni�ciu przycisku myszy
 */
void Widget::mousePressEvent(QMouseEvent *event){
	myszX=((event->pos().x())*128)/(width()-ui->output->width());
	myszY=(event->pos().y()*64)/height();
}
/*
 * Pobierz pozycj� zwolnienia kursora myszy, ogranicz warto�ci i zamie� na po�o�enie wzgl�dne
 * Gdy pozycja jest ta sama, wy�wietl tylko X, Y; Gdy si� zmieni, poka� dodatkowo dX, dY.
 * Ustaw odpowiednie przesuni�cia, �eby �adnie wygl�da�o.
 * TODO: ustawianie kursora na z g�ry ustalon� pozycj� w osi x
 */
void Widget::mouseReleaseEvent(QMouseEvent *event){
	short deltaX=((event->pos().x())*128)/(width()-ui->output->width()),
			deltaY=(event->pos().y()*64)/height();

	if(myszX>=128||myszY>=64) return;
	if(deltaX<0) deltaX=0;
	else if(deltaX>127) deltaX=127;
	if(deltaY<0) deltaY=0;
	else if(deltaY>127) deltaY=127;
	deltaX-=myszX;
	deltaY-=myszY;

	QString msg="x:"+QString::number(myszX);
	if(myszX<10) msg+='\t';
	msg+="\ty:"+QString::number(myszY);
	if(myszY<10) msg+='\t';
	if (deltaX||deltaY){
		msg+="\tdx:"+QString::number(deltaX);
//		if(deltaX<10&&deltaX>=0) msg+='\t';
		msg+="\tdy:"+QString::number(deltaY);
	}
	processTextEmit(msg);
}
/*
 * Skaluj pixmap� do rozmiaru okna
 */
void Widget::paintEvent(QPaintEvent *event){
	short w = this->width();
	short h = this->height();
ui->label->setPixmap (pix->scaled (w,h,Qt::KeepAspectRatio));
	Q_UNUSED(event)
}
/*
 * Skaluj okno za pomoc� rolki myszy
 */
void Widget::wheelEvent(QWheelEvent *event){
	if (event->delta() > 0)
		resize(size()*1.1);
	else
		resize(size()/1.1);
	Q_UNUSED(event)
}
/*
 * 	Zachowaj aspect ratio podczas zmiany rozmiaru okna
 *	2:1 dla pixmapy plus sta�a szeroko�� dla QTextBrowser
 */
void Widget::resizeEvent(QResizeEvent *event){
	short wid=this->width();
	resize(wid, (wid-ui->output->width())/2);
	Q_UNUSED(event)
}

/*	SLOT
 * Nadpisz pixmap� ze �r�d�a, wywo�ywane przez ssd1306_displayLine z myThreada
 * Uaktualnia poziomy rz�d dla Y nale��ce do <line*8;(line+1)*8), gdzie line nale�y do <0;7>
 */
void Widget::updateDisplay(quint8 *src, quint8 line){
	QPainter paint(pix);
	src+=line*128;
		for (int w = 0; w < 128; ++w) {				//warto�� x
			uint8_t hlp=*(src++);
			for (int bit = 0; bit < 8; ++bit) {		//pozycja w bajcie
				if(hlp&(1<<bit))
					paint.setPen(Qt::cyan);
				else paint.setPen(Qt::black);
					paint.drawPoint(w,line*8+bit);
			}

	}
		update();
}
/*	SLOT
 * Wy�lij lini� tekstu do QTextBrowser
 */
void Widget::processText(QString msg){
	ui->output->moveCursor(QTextCursor::End);
	ui->output->insertPlainText(msg+"\n");

	QScrollBar *scroll = ui->output->verticalScrollBar();
	scroll->setValue(scroll->maximum());
}

void Widget::on_butt1_clicked()
{
	emit trd->buttonPressed(1);
}

void Widget::on_butt2_clicked()
{
	emit trd->buttonPressed(2);
}

void Widget::on_butt3_clicked()
{
	emit trd->buttonPressed(4);
}

void Widget::on_butt4_clicked()
{
	emit trd->buttonPressed(8);
}

void Widget::on_input_returnPressed()
{
	if(ui->input->text().contains("cls",Qt::CaseInsensitive))
		ui->output->clear();
	else emit trd->processCmd(ui->input->text());
	ui->input->clear();
}

void Widget::keyPressEvent(QKeyEvent *event){
	switch (event->key()) {
		case Qt::Key_Up:
			emit trd->buttonPressed(1);
			break;
		case Qt::Key_Down:
			emit trd->buttonPressed(2);
			break;
//		case Qt::Key_Left:
//			emit trd->buttonPressed(4);
//			break;
//		case Qt::Key_Right:
//			emit trd->buttonPressed(8);
//			break;
		default:
			break;
	}
	event->accept();
}
