#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "myThread.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
	Q_OBJECT

public:
	explicit Widget(QWidget *parent = 0);
	~Widget();
public slots:
	void updateDisplay(quint8 *src, quint8 line);
	void processText(QString msg);
signals:
	void processTextEmit(QString msg);
private slots:
	void on_butt1_clicked();

	void on_butt2_clicked();

	void on_butt3_clicked();

	void on_butt4_clicked();

	void on_input_returnPressed();

private:
	myThread *trd;
	Ui::Widget *ui;
	QPixmap *pix;
	unsigned short myszX, myszY;

	void paintEvent(QPaintEvent *event);
	void wheelEvent(QWheelEvent *event);
	void resizeEvent(QResizeEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void keyPressEvent(QKeyEvent *event);

};

#endif // WIDGET_H
