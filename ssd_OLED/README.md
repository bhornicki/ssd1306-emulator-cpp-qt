# ssd1306_OLED
My code to control I2C (and SPI, but that's not tested)  ssd1306 OLED display. Tested with 128x64 I2C one.
Comments are mainly written in Polish. 

Lib is based on Adafruit sources. 

# Description of files
## ssd1306
Contains pin config, command defines and functions to speak with SSD1306 controller.

## graphics_mono
Here lays frame buffer and its size defines. Also majority of functions here return error status.

## fonts
Some functions to write text to the display.

# emulator folder
Delete it if you just want to run code on AVR.

Project is powered by QT framework.

Those files deserve to be rewritten. But they work! :D
## oledEmulator.pro
Copy this file to your main folder and use it to configure QT project.


## main
Initialises emulator.
## emulator
This is translation layer. Allows C code to talk to CPP thread.
## myThread
Contains myThread, which cooperates with emulator.h

File avrMain.h in main folder will be called from here. It should contain avrmain(void) function with your code.
## widget
User interface related things.
