#ifndef _SSD1306_FONTS_H_
#define _SSD1306_FONTS_H_

#include "gfxfont.h"


/*
 * Change it to one of Adafruit GFX-lib fonts, otherwise set to 0 (NULL) to leave default font.
 * How to change?
 * first, include pgmspace. Use function bellow to change font in fly.
 * 	gfxFont=(GFXfont *)&FreeSans9pt7b;
 */
extern GFXfont  *gfxFont;

uint8_t ssd1306_char(int16_t x, int16_t y, unsigned char c, uint8_t size,  uint8_t color);

int16_t ssd1306_string(int16_t x, int16_t y, const char *str,
					   uint8_t size, int8_t interspace, uint8_t color,
					   uint8_t source);
int16_t ssd1306_stringLimited(int16_t x, int16_t y, const char *str,
							  uint8_t size, int8_t interspace, uint8_t color,
							  uint8_t maxLength, uint8_t source);

uint8_t ssd1306_getStringLength(char *string, uint8_t source);
int16_t ssd1306_getStringWidth(char *string, int8_t size, int8_t interspace, uint8_t source);

#define ssd1306__stdCharWidth 5
#define ssd1306__stdCharHeight 5

#endif /*	_SSD1306_FONTS_H_	*/
