#-------------------------------------------------
#
# Project created by QtCreator 2018-02-16T22:12:08
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = oledEmulator
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
		avrmain.c \
        ssd_OLED/emulator/main.cpp \
        ssd_OLED/emulator/widget.cpp \
		ssd_OLED/emulator/emulator.cpp \
		ssd_OLED/emulator/myThread.cpp \
		ssd_OLED/fonts.c \
		ssd_OLED/graphics_mono.c \
		ssd_OLED/ssd1306.c \
    hhvcore.c

HEADERS += \
		avrmain.h \
        ssd_OLED/emulator/widget.h \
		ssd_OLED/emulator/emulator.h \
		ssd_OLED/emulator/myThread.h \
		ssd_OLED/Fonts/FreeMono12pt7b.h \
		ssd_OLED/Fonts/FreeMono18pt7b.h \
		ssd_OLED/Fonts/FreeMono24pt7b.h \
		ssd_OLED/Fonts/FreeMono9pt7b.h \
		ssd_OLED/Fonts/FreeMonoBold12pt7b.h \
		ssd_OLED/Fonts/FreeMonoBold18pt7b.h \
		ssd_OLED/Fonts/FreeMonoBold24pt7b.h \
		ssd_OLED/Fonts/FreeMonoBold9pt7b.h \
		ssd_OLED/Fonts/FreeMonoBoldOblique12pt7b.h \
		ssd_OLED/Fonts/FreeMonoBoldOblique18pt7b.h \
		ssd_OLED/Fonts/FreeMonoBoldOblique24pt7b.h \
		ssd_OLED/Fonts/FreeMonoBoldOblique9pt7b.h \
		ssd_OLED/Fonts/FreeMonoOblique12pt7b.h \
		ssd_OLED/Fonts/FreeMonoOblique18pt7b.h \
		ssd_OLED/Fonts/FreeMonoOblique24pt7b.h \
		ssd_OLED/Fonts/FreeMonoOblique9pt7b.h \
		ssd_OLED/Fonts/FreeSans12pt7b.h \
		ssd_OLED/Fonts/FreeSans18pt7b.h \
		ssd_OLED/Fonts/FreeSans24pt7b.h \
		ssd_OLED/Fonts/FreeSans9pt7b.h \
		ssd_OLED/Fonts/FreeSansBold12pt7b.h \
		ssd_OLED/Fonts/FreeSansBold18pt7b.h \
		ssd_OLED/Fonts/FreeSansBold24pt7b.h \
		ssd_OLED/Fonts/FreeSansBold9pt7b.h \
		ssd_OLED/Fonts/FreeSansBoldOblique12pt7b.h \
		ssd_OLED/Fonts/FreeSansBoldOblique18pt7b.h \
		ssd_OLED/Fonts/FreeSansBoldOblique24pt7b.h \
		ssd_OLED/Fonts/FreeSansBoldOblique9pt7b.h \
		ssd_OLED/Fonts/FreeSansOblique12pt7b.h \
		ssd_OLED/Fonts/FreeSansOblique18pt7b.h \
		ssd_OLED/Fonts/FreeSansOblique24pt7b.h \
		ssd_OLED/Fonts/FreeSansOblique9pt7b.h \
		ssd_OLED/Fonts/FreeSerif12pt7b.h \
		ssd_OLED/Fonts/FreeSerif18pt7b.h \
		ssd_OLED/Fonts/FreeSerif24pt7b.h \
		ssd_OLED/Fonts/FreeSerif9pt7b.h \
		ssd_OLED/Fonts/FreeSerifBold12pt7b.h \
		ssd_OLED/Fonts/FreeSerifBold18pt7b.h \
		ssd_OLED/Fonts/FreeSerifBold24pt7b.h \
		ssd_OLED/Fonts/FreeSerifBold9pt7b.h \
		ssd_OLED/Fonts/FreeSerifBoldItalic12pt7b.h \
		ssd_OLED/Fonts/FreeSerifBoldItalic18pt7b.h \
		ssd_OLED/Fonts/FreeSerifBoldItalic24pt7b.h \
		ssd_OLED/Fonts/FreeSerifBoldItalic9pt7b.h \
		ssd_OLED/Fonts/FreeSerifItalic12pt7b.h \
		ssd_OLED/Fonts/FreeSerifItalic18pt7b.h \
		ssd_OLED/Fonts/FreeSerifItalic24pt7b.h \
		ssd_OLED/Fonts/FreeSerifItalic9pt7b.h \
		ssd_OLED/Fonts/Org_01.h \
		ssd_OLED/Fonts/Picopixel.h \
		ssd_OLED/Fonts/Tiny3x3a2pt7b.h \
		ssd_OLED/Fonts/TomThumb.h \
		ssd_OLED/fonts.h \
		ssd_OLED/gfxfont.h \
		ssd_OLED/graphics_mono.h \
		ssd_OLED/ssd1306.h \
    hhvcore.h

FORMS += \
        ssd_OLED/emulator/widget.ui

DISTFILES += \
    ssd_OLED/README.md
	ssd_OLED/emulator/readMe.txt
